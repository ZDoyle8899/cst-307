.text

	.globl main

main:
	li $v0,4 			            #Code for syscall: print_string
	la $a0, myName 		            #Pointer to string (Load the address of msg)
	syscall				            #Ends the program
	li $v0, 10			            #Explicitly exit the program
	syscall				


.data

	myName: .asciiz "Zack Doyle\n"	#The .asciiz assembler directive creates an ASCII
                                    #string in memory terminated by the null 						
                                    #character. Note that strings are surrounded by 					
                                    #double quotes